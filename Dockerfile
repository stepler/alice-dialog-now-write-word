FROM node:alpine

RUN apk --no-cache --virtual build-dependencies add \
    python \
    make \
    build-base \
    g++ \
    cairo-dev \
    jpeg-dev \
    pango-dev \
    giflib-dev


WORKDIR /app

COPY ./package.json /app
COPY ./src /app/src


RUN npm install

EXPOSE 8888
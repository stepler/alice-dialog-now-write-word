const { json, send } = require('micro');
const { router, get, post } = require('microrouter')

const { BASE_URL } = require('./config')
const scheduler = require('./scheduler')
const { base64, debugTime } = require('./tools')
const { uploadImage, removeImage } = require('./api-dialog')
const generateWord = require('./generate-word')
const generateImage = require('./generate-image')

function getImageUrl(text) {
  return `${BASE_URL}/img/${base64.encode(text)}`
}

async function typeWord(req, res) {
  const { request, session, version } = await json(req);

  const [err, text, tts] = generateWord(request.original_utterance)
  const responseImage = {}

  if (err === null) {
    try {
      const imageId = await uploadImage(getImageUrl(text))
      responseImage.card = {
        type: 'BigImage',
        image_id: imageId,
      }
      scheduler.add(removeImage, [imageId])
    } catch(e) {
      console.error(e)
    }
  }

  const body = {
    version,
    session,
    response: {
      ...responseImage,
      text, 
      tts, 
      end_session: true
    },
  }

  send(res, 200, body)
}

async function imageWord(req, res) {
  const wordB64 = req.params.word

  const word = base64.decode(wordB64)
  const [image, type] = await generateImage(word)

  res.setHeader('Content-Type', type)
  send(res, 200, image)
}

function pingPong(res) {
  send(res, 200, 'pong')
}


scheduler.start(60, 24*60*60)

module.exports = router(
  get('/ping', pingPong), 
  post('/', typeWord), 
  get('/img/:word', imageWord)
)
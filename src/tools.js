
const linearScale = ([domainA, domainB], [rangeA, rangeB]) => ({
  range: domainVal => Math.round(
    rangeA + (rangeB - rangeA) * (domainVal - domainA) / (domainB - domainA)
  ),
  domain: rangeVal => Math.round(
    domainA + (domainB - domainA) * (rangeVal - rangeA) / (rangeB - rangeA)
  ),
})


const base64 = {
  encode: (textStr) => Buffer.from(textStr, 'utf8').toString('base64'),
  decode: (b64Str) => Buffer.from(b64Str, 'base64').toString('utf8')
}

const randomElement = (arr) => {
  const idx = Math.round(Math.random() * (arr.length - 1))
  return arr[idx] || arr[0]
}

const debugTime = (name, cb) => async (...args) => {
  const start = Date.now()
  const res = await cb(...args)
  const time = ((Date.now() - start) * 0.001).toFixed(3)
  console.debug(`${name}: ${time}`)
  return res
}

module.exports = {linearScale, base64, randomElement, debugTime}
const path = require('path')
const { registerFont, createCanvas } = require('canvas')

const { IMG_FONT_PATH, IMG_WIDTH, IMG_HEIGHT } = require('./config')
const { linearScale } = require('./tools')

registerFont(path.join(__dirname, IMG_FONT_PATH), { family: 'WordFont' })
const CANVAS = createCanvas(IMG_WIDTH, IMG_HEIGHT)
const CTX = CANVAS.getContext('2d')


function getFont(fz) {
  return `${fz}px "WordFont"`
}

function clear(ctx) {
  ctx.fillStyle = 'rgb(255, 248, 231)'
  ctx.fillRect(0, 0, IMG_WIDTH, IMG_HEIGHT)
}

function getFontSize(ctx, text) {
  const widthText = (IMG_WIDTH * 0.8) | 0

  ctx.font = getFont(10)
  const width10 = ctx.measureText(text).width
  ctx.font = getFont(50)
  const width50 = ctx.measureText(text).width

  return linearScale([10, 50], [width10, width50]).domain(widthText)
}

function drawText(ctx, text) {
  const fz = getFontSize(ctx, text)

  ctx.font = getFont(fz)
  ctx.textAlign = 'center'
  ctx.textBaseline = 'middle'
  ctx.fillStyle = 'rgb(31, 28, 35)'

  ctx.fillText(text, IMG_WIDTH >>> 1, IMG_HEIGHT >>> 1)
}

module.exports = function(text) {
  clear(CTX)
  drawText(CTX, text)
  return new Promise((resolve, reject) => {
    const type = 'image/jpeg'
    CANVAS.toBuffer(
      (err, buf) => err ? reject(err) : resolve([buf, type]), 
      type, 
      { quality: 0.5 }
    )
  })
}


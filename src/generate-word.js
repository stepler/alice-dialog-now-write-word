const { randomElement } = require('./tools')


function upperFirst(text) {
  return text[0].toUpperCase()+text.slice(1)
}

function extractTextToDisplay(text) {
  const res = text
    .split(/как пишется/i)
    .map(s => s.trim())
    .filter(Boolean)

  if (res.length === 2) {
    return upperFirst(res[1])
  }

  return null
}

const validSpeech = [
  'Вот так',
  'Смотри',
  'Конечно',
  'Без проблем',
  'Не вопрос',
  'Легко!',
  'Как-то так',
  'Гляди',
  'Хорошо, смотри',
]

module.exports = function(input) {
  const text = extractTextToDisplay(input)

  if (text === null) {
    const err = 'Я что-то непоняла, попробуйте еще раз'
    return [new Error(err), err, err]
  }

  return [null, text, randomElement(validSpeech)]
}
const request = require('request-promise-native')

const { DIALOG_ID, DIALOG_OAUTH_TOKEN } = require('./config')


async function uploadImage(url) {
  const res = await request({
    method: 'POST',
    url: `https://dialogs.yandex.net/api/v1/skills/${DIALOG_ID}/images`,
    headers: {
      'Authorization': `OAuth ${DIALOG_OAUTH_TOKEN}`,
      'Content-Type': 'application/json',
    },
    body: { url },
    json: true
  })

  return res.image.id
}

async function removeImage(id) {
  await request({
    method: 'DELETE',
    url: `https://dialogs.yandex.net/api/v1/skills/${DIALOG_ID}/images/${id}`,
    headers: {
      'Authorization': `OAuth ${DIALOG_OAUTH_TOKEN}`,
    }
  })

  return true
}

module.exports = { uploadImage, removeImage }
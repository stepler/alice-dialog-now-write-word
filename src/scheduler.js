
const QUEUE = []
let TIMER = 0
let TIMEOUT = null

function currentTime() {
  return (Date.now() * 0.001) | 0
}

function process() {
  let i = 0
  const now = currentTime()

  while(i < QUEUE.length) {
    const { fn, args, time} = QUEUE[i]

    if (now - time > TIMER) {
      fn(...args)
      QUEUE.splice(i, 1)
    } else {
      i++
    }
  }
}

function start(interval, timer) {
  TIMER = timer
  TIMEOUT = setInterval(process, interval*1000)
  TIMEOUT.unref()
}

function add(fn, args) {
  QUEUE.push({fn, args, time: currentTime() })
}

module.exports = { start, add }
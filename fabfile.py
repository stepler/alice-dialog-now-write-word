#! coding:utf-8
import uuid
import time

from fabric.api import *

PROJECT_PATH = "/root/alice"
PROJECT_BRANCH = "HEAD"

env.roledefs["prod"] = ["root@tmp.ru.net"]


def push():
  tmp_file = "/tmp/%s.tar.gz" % uuid.uuid4().hex

  local("git archive --format=tar.gz -o {output} -9 {branch}".\
      format(output=tmp_file, branch=PROJECT_BRANCH))

  put(tmp_file, tmp_file)

  run("mkdir -p {project_path}".format(project_path=PROJECT_PATH))

  run("tar -xzf {filename} -C {project_path}".\
    format(project_path=PROJECT_PATH, filename=tmp_file))

  run("rm -f {filename}".format(filename=tmp_file))
  local("rm -f {filename}".format(filename=tmp_file))

  with cd(PROJECT_PATH):
    with settings(warn_only=True):
      run("docker-compose stop")

    run("docker-compose build")
    run("docker-compose up -d")

    time.sleep(5)
    run("docker ps -a")

@task
@roles("prod")
def deploy():
  push()
  